var MiogaCantor = Backbone.View.extend ({
	$el:   null,
	mioga: null,
	initialize: function (args) {
		console.log ('[MiogaCantor::initialize]');

		var that   = this;
		this.$el    = args.$el;
		this.mioga = args.mioga;
		var timeSurvey=0;
var nbPendingM;
var nbPendingS;

		
		Handlebars.registerHelper('time', function(key) {
 			var hours = "";
 			var minutes="";
 			hours=Math.floor(key/60);
 			minutes=key%60;
 			if(minutes=="0"){
 				return new Handlebars.SafeString("Durée: "+hours+" h ");
 			}
 			else{
 				return new Handlebars.SafeString("Durée: "+hours+"h"+minutes);	
 			}
  			
		});


		Handlebars.registerHelper('dateString', function(key) {
 			var date = '';

  			var d = new Date(key.substring(0, key.indexOf(' ')));
  			var month = d.getMonth()+1;
  			if(month<10){
  				month='0'+month;
  			}
  			date = 'Crée le ' + d.getDate() + '/' + month + '/' + d.getFullYear();
  			return date;
		});

		Handlebars.registerHelper('dateSurveyModified', function(key) {
 			var date = '';

  			var d = new Date(key.substring(0, key.indexOf(' ')));
  			var month = d.getMonth()+1;
  			if(month<10){
  				month='0'+month;
  			}
  			date = 'Le ' + d.getDate() + '/' + month + '/' + d.getFullYear();
  			return date;
		});

		Handlebars.registerHelper('timeSurveyModified', function(key) {
 			var time = '';
 			var dateProv= key.substring(0,key.indexOf(' '))+'T'+key.substring(key.indexOf(' ')+1);
  			var d = new Date(key.substring(0,key.indexOf(' '))+'T'+key.substring(key.indexOf(' ')+1));
  			var hours = d.getHours();
  			var minutes = d.getMinutes();
  			timeSurvey=hours*60+minutes;
  			if(minutes=='0'){
  				minutes='00';
  			}
  			
  			time = 'De ' + hours+ 'h' + minutes + ' à';
  			return time;
		});

		Handlebars.registerHelper('timeEndSurvey', function(key) {
 			var timeEndSurvey=timeSurvey+key;
 			console.log('timeSurvey : '+timeSurvey);
 			console.log('timeEndSurvey : '+timeEndSurvey);
 			var timeEnd='';
 			var hours = "";
 			var minutes="";
 			hours=Math.floor(timeEndSurvey/60);
 			minutes=timeEndSurvey%60;
 			if(minutes=='0'){
 				minutes='00';
 			}

  			timeEnd = hours+'h'+minutes;
  			return timeEnd;
		});

		Handlebars.registerHelper('creatorModified', function(key) {
 			var creator = 'par ' + key;
			return new Handlebars.SafeString(creator);
		});


		// Register routes for module
		this.mioga.router.route ('cantor', function () {
			getMySurveys();
		});
		

		var getMySurveys = function () {
			console.log ('[MiogaCantor::event] get surveys');
			console.log (that.mioga.get ('user_bin_uri')+'/Cantor/GetActiveSurveys.json');
			$.ajax ({
				type: 'GET',
				url: that.mioga.get ('user_bin_uri')+'/Cantor/GetActiveSurveys.json',
				crossDomain: true,
				xhrFields: {
					withCredentials: true
				},
			
				success: function (data) {
					//map to add key to data
					var count = 0;
					$.map(data.surveys, function(val){
						if(val.progress=='100'){
							val.complete=true;
						}
						else{
							count++;
						}
					});
					nbPendingM = count;
					data.nbPendingM = count;
					data.nbPendingS = nbPendingS;

					// Load template
					var template = Handlebars.compile ($('#cantor-form').html ());

					// Render template
					var $node = $(template (data));
					that.$el.empty ().append ($node);

					$("div[class*='rightDiv']").click(function (event,ui){
						getSolicitationSurveys();
					});

					$('.survey').on('click', function() {
						getSurvey($(this).attr("id"));
					});

					$("i[class*='addButton']").click(function (event,ui){
						that.mioga.router.navigate('createSurvey',{trigger:true});
						//load template
						var template = Handlebars.compile($('#create-survey-form').html());
						console.dir(data);

						//Render template
						var $node = $ (template());
						that.$el.empty().append($node);
					});
				},
				error: function () {

				}
			});
		}

		var getSolicitationSurveys = function() {
			$.ajax ({
				type: 'GET',
				url: that.mioga.get ('user_bin_uri')+'/Cantor/GetSolicitations.json',
				crossDomain: true,
				xhrFields: {
					withCredentials: true
				},
			
				success: function (data) {
					//map to add key to data
					var count = 0;
					$.map(data.surveys, function(val){
						if(val.status){
							val.complete=true;
						}
						else{
							count++;
						}
					});	
					nbPendingS = count;
					data.nbPendingS = count;
					data.nbPendingM = nbPendingM


					// Load template
					var template = Handlebars.compile ($('#cantor-solicitations-form').html ());
					data.nbpending = count;

					// Render template
					var $node = $(template (data));
					that.$el.empty ().append ($node);

					$("i[class*='addButton']").click(function (event,ui){
						that.mioga.router.navigate('createSurvey',{trigger:true});
						//load template
						var template = Handlebars.compile($('#create-survey-form').html());
						console.dir(data);

						//Render template
						var $node = $ (template());
						that.$el.empty().append($node);
					});

					$("div[class*='leftDiv']").click(function (event,ui){
						getMySurveys();
					});

					$('.survey').on('click', function() {
						getSurvey($(this).attr("id"));
					});
				},
				error: function () {

				}
			});
		}

		var getSurvey = function (id) {
			$('.loader').css('display', 'block');
			$.ajax ({
				type: 'GET',
				url: that.mioga.get ('user_bin_uri')+'/Cantor/GetSurvey.json',
				crossDomain: true,
				xhrFields: {
					withCredentials: true
				},
				data: {
					rowid: id
				},
				success: function (data) {

					$.map(data.survey.attendees, function(val){
					console.log('VAL.DATES=')
					console.dir(val.dates);
					console.log(Object.keys(val.dates).length);
					if(Object.keys(val.dates).length >0){
						val.complete=true;
					}
				});

					that.mioga.router.navigate ('surveyDetail', { trigger: true });
					// Load template
					var template = Handlebars.compile ($('#survey-detail-form').html ());
					console.dir(data);

					// Render template

					var $node = $(template (data.survey));
					that.$el.empty ().append ($node);


				},
				error: function () {


				}
			});
		}
	}
});
