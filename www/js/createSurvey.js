var MiogaCreateSurvey = Backbone.View.extend ({
	$el:   null,
	mioga: null,
	initialize: function (args) {
		console.log ('[MiogaCreateSurvey::initialize]');

		var that   = this;
		this.$el    = args.$el;
		this.mioga = args.mioga;

		// Register routes for module

		this.mioga.router.route ('createSurvey', function () {
			console.log ('[MiogaCreateSurvey::event] createSurvey');

			$("i[class*='buttonn']").click(function (event,ui){
				$.ajax ({
					type: 'POST',
					url: that.mioga.get ('user_bin_uri')+'/Cantor/SetSurvey.json',
					crossDomain: true,
					xhrFields: {
						withCredentials: true
					},
					data: {
						attendees:2450,
						attendees:2455,
						dates:$('input[name=date]').val (),	
						//dates:2015-06-22 11:00:00
						description:$('input[name=description]').val (),	
						duration:$('input[name=houre]').val (),
						open_attendee_list:true,
						rowid:25,
						short_url:'https://mioga.alixen.fr/Mioga2/int/url/1247ce9589f647c9e4570f9e737f1332',
						title:$('input[name=title]').val ()
					},
					success: function (data) {
					},
					error: function () {
					}
				});
			});

		});
	}
});



